package utils;

import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.Map;

@Component
public class LightSystem {

    Map<String, Boolean> light = new HashMap<>();

    @PostConstruct
    void init() {
        light.put("kitchen", false);
        light.put("room", false);
        light.put("hallway", false);
        light.put("bathroom", false);
    }

    public void turn(String room, Boolean f) {
        if (light.containsKey(room)) {
            light.put(room, f);
        }
    }
}
