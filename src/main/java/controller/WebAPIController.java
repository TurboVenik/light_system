package controller;


import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import utils.LightSystem;

import java.time.format.DateTimeParseException;

@RestController
@RequestMapping(value = "/api")
public class WebAPIController {

    @Autowired
    LightSystem lightSystem;

    @RequestMapping(value = "/light", method = RequestMethod.POST)
    @ResponseBody
    public ResponseEntity<String> postSample(@RequestBody String body) {
        try {
            JSONParser parser = new JSONParser();
            JSONObject json = (JSONObject)parser.parse(body);
            lightSystem.turn(json.get("room").toString(), Boolean.getBoolean(json.get("mode").toString()));
            return new ResponseEntity<>("Accepted!", null, HttpStatus.ACCEPTED);
        } catch (DateTimeParseException | ParseException e) {
            return new ResponseEntity<>("Something went wrong!", null, HttpStatus.BAD_REQUEST);
        }
    }
}
